<?php

return [
    "CLIENT_ID" => env("MS_OAUTH_CLIENT_ID", ''),
    "CLIENT_SECRET" => env('MS_OAUTH_CLIENT_SECRET') ? 
    	$app->make('_encrypter')->decryptstring(env('MS_OAUTH_CLIENT_SECRET')) : "",
    "TENANT_ID"=> env("MS_OAUTH_TENANT_ID", ''),
    "OFFICE_USER_ID"=> env("MS_OFFICE_USER_ID")
 ];

 