<?php

namespace App\Http\Controllers;

use App\MSConfig;
use App\Repositories\IMSConfigRepository;

class MSConfigController extends Controller
{
    protected $repository;

    public function __construct(IMSConfigRepository $repository)    
    {
        $this->repository = $repository;
    }

    public function getAll() {
        return response()->json($this->repository->getAll());
    }
}
