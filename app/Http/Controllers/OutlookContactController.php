<?php

namespace App\Http\Controllers;

use App\Repositories\IOutlookConfigRepository;
use App\Repositories\IOutlookContactFolderRepository;
use App\Repositories\IOutlookContactRepository;
use App\Services\IOutlookContactService;

class OutlookContactController extends Controller
{
    protected $service;

    public function __construct(IOutlookContactService $service)
    {
        $this->service = $service;
    }

    public function getContact() {
        return response()->json($this->service->findContact(""));
    }


}