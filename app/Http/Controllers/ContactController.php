<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\IContactRepository;

class ContactController extends Controller
{
    protected $repository;

    public function __construct(IContactRepository $repository) 
    {
    	$this->repository = $repository;
    }
    
    public function create(Request $request) 
    {
    	return $this->repository->create($request->json()->all());
    }

    public function getAll() 
    {
        return response()->json($this->repository->getAll());
    }

    public function find($id) 
    {
    	return response()->json($this->repository->find($id));
    }

    public function update($id, Request $request)
    {
    	$this->repository->update($id, $request->json()->all());
    }

    public function delete($id) 
    {
    	$this->repository->delete($id);
    }
}

