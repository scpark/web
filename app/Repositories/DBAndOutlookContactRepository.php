<?php

namespace App\Repositories;

use App\Contact;
use App\Services\IOutlookContactService;
use Illuminate\Database\Eloquent\Collection;

class DBAndOutlookContactRepository implements IContactRepository
{
    protected $model;
    protected $contactService;

    public function __construct(Contact $model, IOutlookContactService $contactService)
    {
        $this->model = $model;
        $this->contactService = $contactService;
    }

    private function convertContactDataToDBContactData($contact_data) {
        $dbContactData = [];
        $dbContactData['last_name'] = $contact_data['lastName'] ?? null;
        $dbContactData['first_name'] = $contact_data['firstName'] ?? null;
        $dbContactData['email'] = $contact_data['email'] ?? null;
        $dbContactData['company_name'] = $contact_data['companyName'] ?? null;
        $dbContactData['mobile_phone'] = $contact_data['mobilePhone'] ?? null;
        $dbContactData['home_phone'] = $contact_data['homePhone'] ?? null;
        $dbContactData['business_phone'] = $contact_data['businessPhone'] ?? null;
        $dbContactData['business_fax'] = $contact_data['businessFax'] ?? null;
        $dbContactData['company_address'] = $contact_data['companyAddress'] ?? null;
        $dbContactData['company_postal_code'] = $contact_data['companyPostalCode'] ?? null;
        $dbContactData['department'] = $contact_data['department'] ?? null;
        $dbContactData['job_title'] = $contact_data['jobTitle'] ?? null;
        $dbContactData['note'] = $contact_data['note'] ?? null;
        $dbContactData['categories'] = $contact_data['categories'] ?? null;

        return $dbContactData;
    }

    public function create(array $contact) : int {
        $dbContact = $this->model->newInstance(
            $this->convertContactDataToDBContactData($contact));

        $dbContact->office_contact_id 
            = $this->contactService->createContact($contact);

        $dbContact->save();

        return $dbContact->id;
    }

    public function getAll() : Collection
    {
        return $this->model->get();
    }

    public function find(int $id) {
        return $this->model::find($id);
    }

    public function update(int $id, array $contact) {
        
        $dbContact = $this->model::find($id);
        $dbContact->update($this->convertContactDataToDBContactData($contact));
        
        $this->contactService->updateContact(
            $dbContact->office_contact_id, $contact);
    }

    public function delete(int $id) {
        $dbContact = $this->model::find($id);

        if ($dbContact != null) {
            $dbContact->delete();
            $this->contactService->deleteContact($dbContact->office_contact_id);
        }
    }
}

