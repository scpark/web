<?php

namespace App\Repositories;

interface IMSConfigRepository
{
    public function getAll();
}