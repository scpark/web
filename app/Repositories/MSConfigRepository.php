<?php

namespace App\Repositories;

use App\MSConfig;

class MSConfigRepository implements IMSConfigRepository
{
    protected $model;

    public function __construct(MSConfig $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->get();
    }
}