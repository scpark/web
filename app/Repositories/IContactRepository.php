<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface IContactRepository
{
	public function create(array $contact) : int;
    public function getAll() : Collection;

    public function find(int $id);
    public function update(int $id, array $contact);
    public function delete(int $id);
}

