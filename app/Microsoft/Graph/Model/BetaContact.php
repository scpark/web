<?php
namespace App\Microsoft\Graph\Model;

class BetaContact extends \Microsoft\Graph\Model\Contact   
{
    public function setPhones($val)
    {
        $this->_propDict["Phones"] = $val;
        return $this;
    }

    public function setPostalAddresses($val)
    {
        $this->_propDict["PostalAddresses"] = $val;
        return $this;
    }
}