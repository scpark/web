<?php

namespace App\Services;

use App\Services\IOutlookContactService;
use App\MSConfig;
use App\Repositories\IMSConfigRepository;

use Illuminate\Support\Facades\Config;
use League\OAuth2\Client\Token\AccessToken;

class OutlookBetaContactService implements IOutlookContactService
{
	const VERSION = "beta";
	protected $msConfigRepository;

	public function __construct(IMSConfigRepository $repository)
	{
		$this->msConfigRepository = $repository;
	}

	private function getAccessTokenFromRepository() 
	{
		$configArray = $this->msConfigRepository->getAll()->toArray();
        $accessToken = "";
        $expires = "";

        foreach($configArray as $config) {
            if ($config['key'] == 'AccessToken') {
                $accessToken = $config['value'];
            }
            else if ($config['key'] == 'Expires') {
                $expires = $config['value'];
            }
        }

        return [$accessToken, $expires];
	}

	private function isValidAccessToken($accessToken, $expires) 
	{
		$isValid = false;
        if ( $accessToken != "") 
        {
            try 
            {
                $token = new AccessToken([
                	"access_token" => $accessToken, 'expires' => $expires]);

                if ($token->hasExpired() == false) 
                {
                    $isValid = true;
                }
            }
            catch(Throwable $e) {}
        }

        return $isValid;
	}

	private function getAccessTokenFromAPI() 
	{
        $provider = new \League\OAuth2\Client\Provider\GenericProvider([
            'clientId' => Config::get('ms.CLIENT_ID'),
            'clientSecret' => Config::get('ms.CLIENT_SECRET'),
            'redirectUri' => '',
            'urlAuthorize' => '',
            'urlAccessToken' => "https://login.microsoftonline.com/" 
            	. Config::get('ms.TENANT_ID') . "/oauth2/token",
            'urlResourceOwnerDetails' => '',
            'scopes' => ''
        ]);

        $token = $provider->getAccessToken('client_credentials', [
            "resource" => "https://graph.microsoft.com/"
        ]);

        return [ $token->getToken(), $token->getExpires()];
	}

	private function saveAccessToken($accessToken, $expires) 
	{
        $config = MSConfig::firstOrNew(['key' => 'AccessToken']);
        $config->value = $accessToken;
        $config->fill(["value"]);
        $config->save();

        $config = MSConfig::firstOrNew(['key' => 'Expires']);
        $config->value = $expires;
        $config->fill(["value"]);
        $config->save();
	}

	private function getAccessToken()
	{
		list($accessToken, $expires) = $this->getAccessTokenFromRepository();
        
        if ($this->isValidAccessToken($accessToken, $expires) == false) 
        {
        	list($accessToken, $expires)= $this->getAccessTokenFromAPI();
			$this->saveAccessToken($accessToken, $expires);
        }

        return $accessToken;
    }

    public function createContact($contact_data) : string
    {
        $accessToken = $this->getAccessToken();

        $url = "/". Config::get("ms.TENANT_ID") . "/users/".
            Config::get("ms.OFFICE_USER_ID") ."/contacts";

        // convert contact_data to outlook contact
        $outlook_contact = 
            $this->convertContactDataToOfficeContact($contact_data);

        // api call
        $graph = new \Microsoft\Graph\Graph();
        $graph->setApiVersion($this::VERSION);

        $ret = $graph->setAccessToken($accessToken)
            ->createRequest("POST", $url)
            ->attachBody($outlook_contact)
            ->setReturnType(\Microsoft\Graph\Model\Contact::class)
            ->execute();

        return $ret->getId();		        
    }

    private function convertContactDataToOfficeContact($contact_data) {
        $outlook_contact = new \App\Microsoft\Graph\Model\BetaContact;

        $outlook_contact->setSurname($contact_data['lastName'] ?? '' );
        $outlook_contact->setGivenName($contact_data['firstName'] ?? '');

        // email
        $email = [];

        $email[] = [
            "name" => ($contact_data["lastName"] ?? ''). " " . $contact_data["firstName"], 
            "address" => $contact_data["email"] ?? ''
        ];

        $outlook_contact->setEmailAddresses($email);

        // phones 
        $phones = [];

        $phones[] = [ 
            "Type" => "Mobile", 
            "Number" => $contact_data["mobilePhone"] ?? ''
        ];

        $phones[] = [ 
            "Type" => "Home", 
            "Number" => $contact_data["homePhone"] ?? ''
        ];

        $phones[] = [ 
            "Type" => "Business", 
            "Number" => $contact_data["businessPhone"] ?? ''
        ];

        $phones[] = [ 
            "Type" => "BusinessFax", 
            "Number" => $contact_data["businessFax"] ?? ''
        ];
        
        $outlook_contact->setPhones($phones);

        // address
        $postalAddresses = [];

        $postalAddresses[] = [
            "type" => "home",
            "street" => $contact_data["homeAddress"] ?? '',
            "postalCode" => $contact_data["homePostalCode"] ?? '',
        ];    

        $postalAddresses[] = [
            "type" => "business",
            "street" => $contact_data["companyAddress"] ?? '',
            "postalCode" => $contact_data["companyPostalCode"] ?? '',
        ];         

        $outlook_contact->setPostalAddresses($postalAddresses);

        // company
        $outlook_contact->setJobTitle($contact_data["jobTitle"] ?? '');
        $outlook_contact->setCompanyName($contact_data["companyName"] ?? '');
        $outlook_contact->setDepartment($contact_data["Department"] ?? '');
        $outlook_contact->setPersonalNotes($contact_data["Note"] ?? '');
        $outlook_contact->setCategories(explode(";", $contact_data["categories"] ?? ''));

        return $outlook_contact;
    }

    public function findContact($contact_id) : array
    {
        $accessToken = $this->getAccessToken();

        $url = "/". Config::get("ms.TENANT_ID") . "/users/".
            Config::get("ms.OFFICE_USER_ID") ."/contacts/". $contact_id;

        $graph = new \Microsoft\Graph\Graph();
        $graph->setApiVersion($this::VERSION);

        $contact = $graph->setAccessToken($accessToken)
            ->createRequest("GET", $url)
            ->setReturnType(\Microsoft\Graph\Model\Contact::class)
            ->execute();

        $contact_data = [
            "lastName" => $contact->getSurname(),
            "firstName" => $contact->getGivenName(),
            "email" => $contact->getEmailAddresses()[0]['address'] ?? '',
            "jobTitle" => $contact->getJobTitle(),
            "categories" => implode(";", $contact->getCategories()),
        ];
        
        foreach ($contact->getProperties()["phones"] as $phone) {
            switch ($phone["type"]) {
                case "mobile":
                    $contact_data['mobilePhone'] = $phone["number"];
                    break;
                case "home" :
                    $contact_data['homePhone'] = $phone["number"];
                    break;
                case "business" :
                    $contact_data['businessPhone'] = $phone["number"];
                    break;
                case "businessFax" :
                    $contact_data['businessFax'] = $phone["number"];
            }
        }        

        return $contact_data;
    }

    public function updateContact($contact_id, $contact_data)
    {
        $accessToken = $this->getAccessToken();

        $url = "/". Config::get("ms.TENANT_ID") . "/users/".
            Config::get("ms.OFFICE_USER_ID") . "/contacts/". $contact_id;

        // convert contact_data to outlook contact
        $outlook_contact = 
            $this->convertContactDataToOfficeContact($contact_data);

        // api call
        $graph = new \Microsoft\Graph\Graph();
        $graph->setApiVersion($this::VERSION);

        $graph->setAccessToken($accessToken)
            ->createRequest("PATCH", $url)
            ->attachBody($outlook_contact)
            ->setReturnType(\Microsoft\Graph\Model\Contact::class)
            ->execute();
    }

    public function deleteContact($contact_id)
    {
        $accessToken = $this->getAccessToken();

        $url = "/". Config::get("ms.TENANT_ID") . "/users/".
            Config::get("ms.OFFICE_USER_ID") ."/contacts/". $contact_id;

        // api call
        $graph = new \Microsoft\Graph\Graph();
        $graph->setApiVersion($this::VERSION);

        $graph->setAccessToken($accessToken)
            ->createRequest("DELETE", $url)
            ->setReturnType(\Microsoft\Graph\Model\Contact::class)
            ->execute();    
    }
}
