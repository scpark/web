<?php

namespace App\Services;

interface IOutlookContactService
{
    public function createContact($contact_data) : string;
    public function findContact($contact_id) : array;
    public function updateContact($contact_id, $contact_data);
    public function deleteContact($contact_id);
}