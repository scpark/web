<?php
namespace App\Bootstrap;

use Illuminate\Contracts\Foundation\Application;

class EnableEncryptInConfiguration
{
	public function bootstrap(Application $app)
    {
    	$config = require $app->configPath().DIRECTORY_SEPARATOR."app.php";

    	if (!is_null($config['key']))
    	{
			if (\Illuminate\Support\Str::startsWith($key = $config['key'], 'base64:')) {
			    $key = base64_decode(substr($key, 7));
			} 

			$encrypter = new \Illuminate\Encryption\Encrypter($key, $config['cipher']);

			$app->instance('_encrypter', $encrypter);
		}
    }
}
    
    