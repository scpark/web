<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
	use SoftDeletes;

    protected $fillable = [
        'office_contact_id',
        'last_name',
        'first_name',
        'email',
        'company_name',
        'mobile_phone',
        'home_phone',
        'business_phone',
        'business_fax',
        'company_address',
        'company_postal_code',
        'department',
        'job_title',
        'note',
        'categories',
    ];
    
}

