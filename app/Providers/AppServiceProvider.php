<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\MSConfig;
use App\Repositories\IMSConfigRepository;
use App\Repositories\MSConfigRepository;

use App\Services\IOutlookContactService;
use App\Services\OutlookBetaContactService;

use App\Contact;
use App\Repositories\IContactRepository;
use App\Repositories\DBAndOutlookContactRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        //
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /** MSConfig **/
        $this->app->bind(MSConfig::class, function ($app){
            return new MSConfig();
        });


        $this->app->singleton(IMSConfigRepository::class, function($app){
            return new MSConfigRepository($app->make('App\MSConfig'));
        });

        /** MSOutlook **/
        $this->app->singleton(IOutlookContactService::class, function($app){
            return new OutlookBetaContactService($app->make('App\Repositories\IMSConfigRepository'));
        });

        /** Contact */
        $this->app->bind(Contact::class, function($app) {
            return new Contact();
        });
                             
        $this->app->singleton(IContactRepository::class, function($app) {
            return new DBAndOutlookContactRepository($app->make('App\Contact'), $app->make('App\Services\IOutlookContactService'));
        });
    }
}
