<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSConfig extends Model
{
	protected $table = "MSConfigs";
	
    protected $fillable = ["key", "value"];
}
