<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

use App\MSConfig;
use App\Contact;
use App\Repositories\MSConfigRepository;
use App\Services\OutlookBetaContactService;
use App\Repositories\DBAndOutlookContactRepository;

class DBAndOutlookContactRepositoryTest extends TestCase
{

    public function testContactCRUD()
    {
        
        $configRepository = new MSConfigRepository(new MSConfig);
        $service = new OutlookBetaContactService($configRepository);
        $repository = new DBAndOutlookContactRepository(new Contact, $service);

        $contact_data = [
            "lastName" => "박",
            "firstName" => "세창",
            "email" => "threewindow@hanmail.net",
            "mobilePhone" => "010-0000-0000",
            "homePhone" => "010-1111-1111",
            "businessPhone" => "010-2222-2222",
            "businessFax" => "010-3333-3333",
            "companyName" => "프리랜서",
            "homeAddress" => "집주소",
            "companyAddress" => "회사주소",
            "categories" => "a;b;c",
            "abcd" => "abcd",
        ];

        $id = $repository->create($contact_data);
        $this->assertNotNull($id);

        $find_contact_data = $repository->find($id);

        $this->assertEquals($contact_data["lastName"], $find_contact_data->last_name);
        $this->assertEquals($contact_data["firstName"], $find_contact_data->first_name);
        $this->assertEquals($contact_data["email"], $find_contact_data->email);
        $this->assertEquals($contact_data["mobilePhone"], $find_contact_data->mobile_phone);
        $this->assertEquals($contact_data["homePhone"], $find_contact_data->home_phone);
        $this->assertEquals($contact_data["businessPhone"], $find_contact_data->business_phone);
        $this->assertEquals($contact_data["businessFax"], $find_contact_data->business_fax);
        $this->assertEquals($contact_data["categories"], $find_contact_data->categories);

        $update_contact_data = [
            "lastName" => "Park",
            "firstName" => "Sechang",
            "jobTitle" => "Developer",            
            "email" => "threewindow@knou.ac.kr",
            "companyName" => "free",
        ];

        $repository->update($id, $update_contact_data);

        $find_updated_contact_data = $repository->find($id);
        $this->assertEquals($update_contact_data["lastName"], $find_updated_contact_data->last_name);
        $this->assertEquals($update_contact_data["firstName"], $find_updated_contact_data->first_name);
        $this->assertEquals($update_contact_data["email"], $find_updated_contact_data->email);
        $this->assertEquals($update_contact_data["mobilePhone"] ?? "", $find_updated_contact_data->mobile_phone);
        $this->assertEquals($update_contact_data["homePhone"] ?? "", $find_updated_contact_data->home_phone);
        $this->assertEquals($update_contact_data["businessPhone"] ?? "", $find_updated_contact_data->business_phone);
        $this->assertEquals($update_contact_data["businessFax"] ?? "", $find_updated_contact_data->business_fax);
        $this->assertEquals($update_contact_data["jobTitle"], $find_updated_contact_data->job_title);
        $this->assertEquals($update_contact_data["categories"] ?? "", $find_updated_contact_data->categories);
       
        $all = $repository->getAll();
        $this->assertNotNull($all);
        $cnt = count($all);

        $ret = $repository->delete($id);

        $all = $repository->getAll();
        $this->assertNotNull($all);
        $this->assertEquals($cnt - 1, count($all));
       
    }

}
