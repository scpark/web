<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;

use App\MSConfig;
use App\Repositories\MSConfigRepository;
use App\Services\OutlookBetaContactService;

class OutlookBetaContactServiceTest extends TestCase
{

    public function testContactCRUD()
    {
        
        $repository = new MSConfigRepository(new MSConfig);
        $service = new OutlookBetaContactService($repository);

        $contact_data = [
            "lastName" => "박",
            "firstName" => "세창",
            "email" => "threewindow@hanmail.net",
            "mobilePhone" => "010-0000-0000",
            "homePhone" => "010-1111-1111",
            "businessPhone" => "010-2222-2222",
            "businessFax" => "010-3333-3333",
            "companyName" => "프리랜서",
            "homeAddress" => "집주소",
            "companyAddress" => "회사주소",
            "categories" => "a;b;c",
        ];

        $id = $service->createContact($contact_data);
        $this->assertNotNull($id);

        $find_contact_data = $service->findContact($id);

        $this->assertEquals($contact_data["lastName"], $find_contact_data["lastName"]);
        $this->assertEquals($contact_data["firstName"], $find_contact_data["firstName"]);
        $this->assertEquals($contact_data["email"], $find_contact_data["email"]);
        $this->assertEquals($contact_data["mobilePhone"], $find_contact_data["mobilePhone"]);
        $this->assertEquals($contact_data["homePhone"], $find_contact_data["homePhone"]);
        $this->assertEquals($contact_data["businessPhone"], $find_contact_data["businessPhone"]);
        $this->assertEquals($contact_data["businessFax"], $find_contact_data["businessFax"]);
        $this->assertEquals($contact_data["categories"], $find_contact_data["categories"]);

        $update_contact_data = [
            "lastName" => "Park",
            "firstName" => "Sechang",
            "jobTitle" => "Developer",            
            "email" => "threewindow@knou.ac.kr",
            "companyName" => "free",
        ];

        $service->updateContact($id, $update_contact_data);

        $find_updated_contact_data = $service->findContact($id);
        $this->assertEquals($update_contact_data["lastName"], $find_updated_contact_data["lastName"]);
        $this->assertEquals($update_contact_data["firstName"], $find_updated_contact_data["firstName"]);
        $this->assertEquals($update_contact_data["email"], $find_updated_contact_data["email"]);
        $this->assertEquals($update_contact_data["mobilePhone"] ?? "", $find_updated_contact_data["mobilePhone"]);
        $this->assertEquals($update_contact_data["homePhone"] ?? "", $find_updated_contact_data["homePhone"]);
        $this->assertEquals($update_contact_data["businessPhone"] ?? "", $find_updated_contact_data["businessPhone"]);
        $this->assertEquals($update_contact_data["businessFax"] ?? "", $find_updated_contact_data["businessFax"] ?? "");
        $this->assertEquals($update_contact_data["jobTitle"], $find_updated_contact_data["jobTitle"]);
        $this->assertEquals($update_contact_data["categories"] ?? "", $find_updated_contact_data["categories"] ?? "");
       $ret = $service->deleteContact($id);
    }

}
