<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('office_contact_id', 250)->nullable(true);
            $table->string('last_name', 50)->nullable(true);
            $table->string('first_name', 50)->nullable(true);
            $table->string('email',200)->nullable(true);
            $table->string('company_name',100)->nullable(true);
            $table->string('mobile_phone',50)->nullable(true);
            $table->string('home_phone',50)->nullable(true);
            $table->string('business_phone',50)->nullable(true);
            $table->string('business_fax',50)->nullable(true);
            $table->string('company_address',2000)->nullable(true);
            $table->string('company_postal_code',10)->nullable(true);
            $table->string('department',100)->nullable(true);
            $table->string('job_title',100)->nullable(true);
            $table->string('categories',100)->nullable(true);
            $table->mediumText('note')->nullable(true);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}

